<?php
session_start();

use Settings\Connection;

require_once("../../settings/Connection.php");
require_once("../../models/Bill.php");
require_once("../../models/Book.php");
require_once("../../models/User.php");
require_once("../../models/BillContent.php");
require_once("../../settings/functions.php");

if (!auth()) {
    // not auth
    header('Location: ../auth/login.php?errors[]=you+have+to+be+logged+in+first');
} else {
    if (!admin()) {
        // not admin
        header('Location: ../Book/index.php?errors[]=you+don\'t+have+a+permission');
    }
}

extract($_GET);

$uid = auth();
$time = date('Y-m-d H:i:s');


$query = "SELECT `id` FROM `bill` WHERE `user_id`={$uid} ORDER BY `id` DESC LIMIT 1";

try {
    $conn = Connection::connect();
    $bill = $conn->query($query);
    $bill = $bill->fetch();
  } catch (PDOException $e) {
    echo "error: " . $e->getMessage();
}

if(empty($bill)){

    $query = "INSERT INTO `bill`(`user_id`, `timestamp`, `status`) VALUES ('{$uid}','{$time}','0')";

    try {
        $conn = Connection::connect();
        $conn->query($query);
      } catch (PDOException $e) {
        echo "error: " . $e->getMessage();
      }

      $query = "SELECT `id` FROM `bill` WHERE `user_id`={$uid} ORDER BY `id` DESC LIMIT 1";

    try {
        $conn = Connection::connect();
        $bill = $conn->query($query);
        $bill = $bill->fetch();
        $id = $bill['id'];
      } catch (PDOException $e) {
        echo "error: " . $e->getMessage();
      }

      $query = "INSERT INTO `bill_contents`(`bill_id`, `book_id`, `quantity`) VALUES ('{$id}','{$book_id}','1')";

        try {
            $conn = Connection::connect();
            $conn->query($query);
          } catch (PDOException $e) {
            echo "error: " . $e->getMessage();
        }

        $query = "DELETE FROM `cart` WHERE `book_id`={$book_id}";

        try {
            $conn = Connection::connect();
            $conn->query($query);
          } catch (PDOException $e) {
            echo "error: " . $e->getMessage();
        }

    }else {
        $id = $bill['id'];

        $query = "INSERT INTO `bill_contents`(`bill_id`, `book_id`, `quantity`) VALUES ('{$id}','{$book_id}','1')";

        try {
            $conn = Connection::connect();
            $conn->query($query);
          } catch (PDOException $e) {
            echo "error: " . $e->getMessage();
        }

        $query = "DELETE FROM `cart` WHERE `book_id`={$book_id}";

        try {
            $conn = Connection::connect();
            $conn->query($query);
          } catch (PDOException $e) {
            echo "error: " . $e->getMessage();
        }


    }
    
    header('Location: index.php?success[]=Book+Reserved+!');



?>
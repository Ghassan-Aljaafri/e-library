<?php
session_start();

use Models\Author;
use Models\Cart;
use Models\Category;
use Settings\Connection;

require '../layout/header.php';
require '../layout/nav.php';
require '../layout/sidebar.php';

require_once("../../settings/Connection.php");
require_once("../../models/Book.php");
require_once("../../models/Author.php");
require_once("../../models/Category.php");
require_once("../../models/User.php");
require_once("../../models/Cart.php");
require_once("../../settings/functions.php");

$authors = new Author();
$authors = $authors->index();
$categories = new Category();
$categories = $categories->index();
$cart_books = new Cart();
$cart_books = $cart_books->index();

if (!auth()) {
  ?>
    <script type="text/javascript">
      window.location.href = '../auth/login.php?errors[]=you+have+to+be+logged+in+first';
    </script>
  <?php
    // header('Location: ../Book/index.php?errors[]=you+have+to+be+logged+in');
}

$user_id = auth();

$stmt = "SELECT 
    `books`.`id`,`title`, `price`, `quantity`, `image`, `categories`.`id` AS `c_id` ,`categories`.`c_name` ,`authors`.`id` AS `a_id` ,`authors`.`a_name`
FROM
    `books`
INNER JOIN `categories` ON `books`.`category_id` = `categories`.`id`
INNER JOIN `authors` ON `books`.`author_id` = `authors`.`id`
INNER JOIN `cart` ON `books`.`id` = `cart`.`book_id`
WHERE `user_id` = '{$user_id}'";

if (isset($_GET['search'])) {
  extract($_GET);

  if ($category != "*") {
    $stmt .= " AND `category_id`='{$category}' ";
  }

  if ($author != "*") {
    $stmt .= " AND `author_id`='{$author}' ";
  }
}
$books=[];
try {
  $conn = Connection::connect();
  $books = $conn->query($stmt);
  $books = $books->fetchAll();
} catch (PDOException $e) {
  echo "error: " . $e->getMessage();
}

?>
<section class="container">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark"><i class="fa fa-shopping-cart"></i> My Cart</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">My Cart</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <?php getMessages(); ?>

  <div class="search-box">
    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="GET">
      <div class="row">
        <div class="form-group col col-6">
          <label for="">author</label>
          <select class="form-control" name="author" id="">
            <option>*</option>
            <?php foreach ($authors as $author) : ?>
              <option class="form-control" value="<?php echo $author['id']; ?>"> <?php echo $author['a_name']; ?></option>
            <?php endforeach ?>
          </select>
        </div>
        <div class="form-group col col-6">
          <label for="">category</label>
          <select class="form-control" name="category" id="">
            <option>*</option>
            <?php foreach ($categories as $category) : ?>
              <option class="form-control" value="<?php echo $category['id']; ?>"> <?php echo $category['c_name']; ?></option>
            <?php endforeach ?>
          </select>
        </div>
      </div>
      <input class="btn btn-primary float-right" type="submit" name="search" value="search">
      <div class="clearfix"></div>
    </form>
  </div>
  <hr>
  <!-- Main content -->

  <?php  if (count($books)) : ?>
    <div class="row">
      <?php foreach ($books as $book) : ?>
        <div class="col-lg-3 col-md-4 col-sm-6 col-12">
          <div class="card ">
            <img class="card-img-top" src="../../images/books/<?= $book['image'] ?>" />
            <div class="card-body">
              <h5 class="card-title"><?= $book['title']; ?></h5>
              <br><hr>
              <!-- <p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p> -->
              <span class="card-text ">
                <strong class="text-primary"><?= $book['price'] . "  LYD"; ?></strong>
                <small class="text-muted"><?= $book['quantity'] . " Copy Available"; ?></small>
              </span>
              <hr>
              <form action="reserveAll.php" method="POST">
              <div>
                  <a href="delete.php?book_id=<?= $book['id'] ?>" class="btn btn-sm btn-danger" title="Delete" ><i class="fa fa-trash"></i></a>
                  <a href="reserve.php?book_id=<?= $book['id'] ?>" class="btn btn-sm btn-success" title="Reserve" ><i class="fa fa-plus-circle"></i></a>
              </div>
              <!-- <span class="badge badge-success"><i class="fa fa-money"></i> <?= $book['price'] ?></span> -->
              <?php // if(auth()): ?>
                <!-- <form action="/add-to-cart" >
                  <div class="row">
                    <input type="hidden" name="id" value="<?= $book['id'] ?>">
                    <div class=" col-lg-10 col-md-12 col-sm-10 col-10">
                      <input class="form-control" type="number" name="quantity" id="" min="1" max="<?= $book['quantity'] ?>" placeholder="Quantity">
                    </div>
                    <button class="btn  col-lg-2 col-md-12 col-sm-2 col-2 btn-success" type="submit"><i class="fa fa-cart-plus"></i></button>
                  </div>
                </form> -->
              <?php // endif; ?>
            </div>
            <div class="card-footer">
              <span class="badge badge-info"><i class="fa fa-pen"></i> <?= $book['a_name'] ?></span>
              <span class="badge badge-primary"><i class="fa fa-bookmark"></i> <?= $book['c_name'] ?></span>
            </div>
          </div>
        </div>
      <?php endforeach; ?>
    </div>
    <?php  if (count($cart_books)) : ?>
      <hr>
        <button type="submit" class="btn btn-primary" name="reserve" value="reserve">Reserve All Books in my Cart</button>
      <hr>
    <?php endif; ?>
  </form>
    <?php else: ?>
      <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
          <span class="sr-only">Close</span>
        </button>
        <strong>Sorry!</strong> No items in your cart.
      </div>
    <?php endif; ?>
</section>
<!-- /.content -->
<?php require '../layout/footer.php'  ?>
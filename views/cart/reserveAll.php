<?php
session_start();

use Settings\Connection;

require_once("../../settings/Connection.php");
require_once("../../models/Bill.php");
require_once("../../models/Book.php");
require_once("../../models/User.php");
require_once("../../models/BillContent.php");
require_once("../../settings/functions.php");

if (!auth()) {
    // not auth
    header('Location: ../auth/login.php?errors[]=you+have+to+be+logged+in+first');
} else {
    if (!admin()) {
        // not admin
        header('Location: ../Book/index.php?errors[]=you+don\'t+have+a+permission');
    }
}

extract($_POST);

$uid = auth();
$time = date('Y-m-d H:i:s');

$query = "SELECT `book_id` FROM `cart` WHERE `user_id`={$uid}";

try {
    $conn = Connection::connect();
    $cart_books = $conn->query($query);
    $cart_books = $cart_books->fetchAll();
  } catch (PDOException $e) {
    echo "error: " . $e->getMessage();
}
    // var_dump($cart_books);
    //die(var_dump($cart_books));

$query = "INSERT INTO `bill`(`user_id`, `timestamp`, `status`) VALUES ('{$uid}','{$time}','0')";

try {
    $conn = Connection::connect();
    $conn->query($query);
  } catch (PDOException $e) {
    echo "error: " . $e->getMessage();
  }

  $query = "SELECT `id` FROM `bill` ORDER BY `id` DESC LIMIT 1";

  try {
    $conn = Connection::connect();
    $id = $conn->query($query);
    $id = $id->fetch();
    $id = $id['id']; 
  } catch (PDOException $e) {
    echo "error: " . $e->getMessage();
  }

  foreach($cart_books as $item) {

    $bid = $item['book_id'];

    $query = "INSERT INTO `bill_contents`(`bill_id`, `book_id`, `quantity`) VALUES ('{$id}','{$bid}','1')";
    try {
        $conn = Connection::connect();
        $conn->query($query);
      } catch (PDOException $e) {
        echo "error: " . $e->getMessage();
      }

    $query = "DELETE FROM `cart` WHERE `book_id`={$bid}";

      try {
          $conn = Connection::connect();
          $conn->query($query);
        } catch (PDOException $e) {
          echo "error: " . $e->getMessage();
      }

  }

  header('Location: index.php?success[]=All+Books+in+Your+cart+has+been+Reserved+!');






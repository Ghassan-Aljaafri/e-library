<?php
session_start();

require_once("../../settings/Connection.php");
require_once("../../settings/functions.php");

use Settings\Connection;

if (auth()) {
    try {
        $user_id = auth();
        $stmt = "DELETE FROM  `cart` WHERE `user_id` = '{$user_id}' AND `book_id` = '{$_GET['book_id']}'";
        $connection = Connection::connect();
        $result = $connection->exec($stmt);
        
        if ($result) {
            header('Location: index.php?success[]=book+deleted+from+your+cart+successfully');
        }
    } catch(PDOException $e) {
        echo $stmt . "<br>" . $e->getMessage();
    }

} else {
    // not auth
    header('Location: ../auth/login.php?errors[]=you+have+to+be+logged+in+first');
}
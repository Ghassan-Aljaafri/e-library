<?php
session_start();

use Settings\Connection;

require("../../settings/functions.php");
require("../../settings/Connection.php");

if (auth()) {
    $user_id = auth();
    
    $stmt = "SELECT * FROM `cart` WHERE `user_id` = '$user_id' AND `book_id` = {$_GET['book_id']}";
    $connection = Connection::connect();
    $results = $connection->query($stmt);
    $carts = $results->fetchAll();
    if (count($carts)) {
        header('Location: ../Book/index.php?errors[]=this+book+already+in+cart');
    } else {
        $stmt = "INSERT INTO `cart`(`book_id`, `user_id`) VALUES ('{$_GET['book_id']}','{$user_id}')";
        try {
            $conn = Connection::connect();
            $results = $conn->query($stmt);
            
            header('Location: ../Book/index.php?success[]=added+to+your+cart+successfully');
        } catch (PDOException $e) {
            echo "error: " . $e->getMessage();
        }
    }
} else {
    header('Location: ../auth/login.php?errors[]=you+have+to+be+logged+in+first');
}
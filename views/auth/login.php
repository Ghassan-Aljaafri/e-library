<?php
 
//login.php
 
/**
 * Start the session.
 */
session_start();

require_once("../../settings/functions.php");
if (auth()) {
    // die("already loged in");
    header('Location: ../Book/index.php?errors[]=you+are+already+loged+in');
}

require_once("../../models/Model.php");
require_once("../../models/User.php");

use Models\User;
use Settings\Connection;

/**
 * Include our MySQL connection.
 */
require_once("../../settings/Connection.php");
$conn = Connection::connect();

//If the POST var "login" exists (our submit button), then we can
//assume that the user has submitted the login form.
if ( isset($_POST['login']) ) {
    $errors = [];
    //Retrieve the field values from our login form.
    $email = !empty($_POST['email']) ? trim($_POST['email']) : null;
    $passwordAttempt = !empty($_POST['password']) ? trim($_POST['password']) : null;
    
    //Retrieve the user account information for the given email.
    $user = new User();
    $user = $user->showOne($email, "email");

    //If $row is FALSE.
    if($user === false){
        //Could not find a user with that email!
        //PS: You might want to handle this error in a more user-friendly manner!
        array_push($errors, 'Incorrect email!');
        // die('Incorrect email / password combination!');
    } else {
        //User account found. Check to see if the given password matches the
        //password hash that we stored in our users table.
        
        //Compare the passwords.
        $validPassword = password_verify($passwordAttempt, $user['password']);
        $validPassword = ($passwordAttempt == $user['password'] ? true : false) ;
        
        //If $validPassword is TRUE, the login has been successful.
        if ($validPassword) {
            
            //Provide the user with a login session.
            $_SESSION['user_id'] = $user['id'];
            $_SESSION['logged_in'] = time();
            
            //Redirect to our protected page, which we called home.php
            // TODO
            header('Location: ../Book/index.php');
            exit;
            
        } else {
            //$validPassword was FALSE. Passwords do not match.
            array_push($errors, 'Incorrect password!');
            // die('Incorrect email / password combination!');
        }
    }
}
 
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://adminlte.io/themes/dev/AdminLTE/plugins/fontawesome-free/css/all.min.css">
    <script src="https://kit.fontawesome.com/0555468193.js" crossorigin="anonymous"></script>


    <!------ Include the above in your HEAD tag ---------->
</head>
<body>

<div class="container">
    <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="panel-title">Login</div>
            </div>

            <div style="padding-top:30px" class="panel-body">

                <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
                <?php 
                    if (isset($errors)) {
                        showErrors($errors);
                    }
                    getMessages();
                ?>
                <form action="login.php" method="post" id="loginform" class="form-horizontal" role="form">

                    <div style="margin-bottom: 25px" class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                        <input required id="email" type="email" class="form-control" name="email" placeholder="Email">
                    </div>

                    <div style="margin-bottom: 25px" class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                        <input required id="password" type="password" class="form-control" name="password" placeholder="Password">
                    </div>
                    <div style="margin-top:10px" class="form-group">
                        <!-- Button -->
                        <div class="col-sm-12 controls">
                            <input name="login" value="login" type="submit" id="btn-login" class="btn btn-success">
                            <!-- <a id="btn-fblogin" href="#" class="btn btn-primary">Login with Facebook</a> -->
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12 control">
                            <div style="border-top: 1px solid#888; padding-top:15px; font-size:85%">
                                Don't have an account!
                                <a href="register.php">
                                    Register Here
                                </a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <a href="../Book/index.php" class="btn btn-primary btn-block"><i class="fa fa-home"></i> Home</a>
    </div>
</div>
</body>
</html>
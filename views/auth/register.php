<?php

//register.php

/**
 * Start the session.
 */
session_start();

require_once("../../settings/functions.php");

if (auth()) {
    // die("already loged in");
    header('Location: ../Book/index.php?errors[]=you+are+already+loged+in');
}

require_once("../../models/Model.php");
require_once("../../models/User.php");

use Models\User;
use Settings\Connection;

/**
 * Include our MySQL connection.
 */
require_once("../../settings/Connection.php");
$conn = Connection::connect();

//If the POST var "register" exists (our submit button), then we can
//assume that the user has submitted the registration form.
if (isset($_POST['register'])) {
    $errors = [];
    //Retrieve the field values from our registration form.
    $name = !empty($_POST['name']) ? trim($_POST['name']) : null;
    $username = !empty($_POST['username']) ? trim($_POST['username']) : null;
    $email = !empty($_POST['email']) ? trim($_POST['email']) : null;
    $pass = !empty($_POST['password']) ? trim($_POST['password']) : null;

    //Now, we need to check if the supplied email already exists.

    //Construct the SQL statement and prepare it.
    $sql = "SELECT COUNT(email) AS num FROM users WHERE email = :email";
    $stmt = $conn->prepare($sql);

    //Bind the provided email to our prepared statement.
    $stmt->bindValue(':email', $email);

    //Execute.
    $stmt->execute();

    //Fetch the row.
    $row = $stmt->fetch(PDO::FETCH_ASSOC);

    //If the provided email already exists - display error.
    //TO ADD - Your own method of handling this error. For example purposes,
    //I'm just going to kill the script completely, as error handling is outside
    //the scope of this tutorial.
    if ($row['num'] > 0) {
        array_push($errors, "That email already exists!");
        // die('That email already exists!');
    }

    if (!count($errors)) {
        //Hash the password as we do NOT want to store our passwords in plain text.
        // $passwordHash = password_hash($pass, PASSWORD_BCRYPT, array("cost" => 12));
        $passwordHash = $pass;

        //Prepare our INSERT statement.
        //Remember: We are inserting a new row into our users table.
        $sql = "INSERT INTO `users` VALUES ('null', '{$name}', '{$username}', '{$email}', '{$passwordHash}', '0')";
        $stmt = $conn->prepare($sql);

        try {
            //Execute the statement and insert the new account.
            $result = $stmt->execute();
            //If the signup process is successful.
            if ($result) {
                //What you do here is up to you!

                $user = new User();
                $user = $user->showOne($email, "email");

                //Provide the user with a login session.
                $_SESSION['user_id'] = $user['id'];
                $_SESSION['logged_in'] = time();
                
                //Redirect to our protected page, which we called index.php
                // TODO
                header('Location: ../Book/index.php?success[]=Thank+you+for+registering+with+our+website.');
                exit;

                echo 'Thank you for registering with our website.';
            }
        } catch (\PDOException $e) {
            echo $e->getMessage();
        }
    }
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
    
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://adminlte.io/themes/dev/AdminLTE/plugins/fontawesome-free/css/all.min.css">
    <script src="https://kit.fontawesome.com/0555468193.js" crossorigin="anonymous"></script>


    <!------ Include the above in your HEAD tag ---------->
</head>
<body>

<div class="container">
    <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="panel-title">Register</div>
            </div>

            <div style="padding-top:30px" class="panel-body">

                <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
                <?php 
                    if (isset($errors)) {
                        showErrors($errors);
                    }
                    getMessages();
                ?>
                <form  action="register.php" method="post" id="loginform" class="form-horizontal" role="form">

                    <div style="margin-bottom: 25px" class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
                        <input required id="name" type="text" class="form-control" name="name" placeholder="Name">
                    </div>
                    
                    <div style="margin-bottom: 25px" class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <input required id="username" type="text" class="form-control" name="username" placeholder="Username">
                    </div>

                    <div style="margin-bottom: 25px" class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                        <input required id="email" type="email" class="form-control" name="email" placeholder="Email">
                    </div>

                    <div style="margin-bottom: 25px" class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                        <input required id="password" type="password" class="form-control" name="password" placeholder="Password">
                    </div>
                    <div style="margin-top:10px" class="form-group">
                        <!-- Button -->
                        <div class="col-sm-12 controls">
                            <input name="register" value="Register" type="submit" id="btn-login" class="btn btn-success">
                            <!-- <a id="btn-fblogin" href="#" class="btn btn-primary">Login with Facebook</a> -->
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12 control">
                            <div style="border-top: 1px solid#888; padding-top:15px; font-size:85%">
                                already have an account
                                <a href="login.php">
                                    Login Here
                                </a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <a href="../Book/index.php" class="btn btn-primary btn-block"><i class="fa fa-home"></i> Home</a>
    </div>
</div>
</body>
</html>
<?php
session_start();

use Models\Author;
use Models\Category;
use Settings\Connection;
use Settings\functions;

require '../layout/header.php';
require '../layout/nav.php';
require '../layout/sidebar.php';


require_once("../../settings/Connection.php");
require_once("../../models/Book.php");
require_once("../../models/Author.php");
require_once("../../models/Category.php");
require_once("../../models/User.php");
require_once("../../settings/functions.php");

$authors = new Author();
$authors = $authors->index();
$categories = new Category();
$categories = $categories->index();

$stmt = "SELECT 
`books`.`id`,`title`, `price`, `quantity`, `image`, `categories`.`id` AS `c_id` ,`categories`.`c_name` ,`authors`.`id` AS `a_id` ,`authors`.`a_name`
FROM
`books`
INNER JOIN `categories` ON `books`.`category_id` = `categories`.`id`
INNER JOIN `authors` ON `books`.`author_id` = `authors`.`id`
WHERE 1=1";

if (isset($_POST['reserve'])) {
  extract($_POST);

  if(empty($Rbooks) || empty($Rbooks2)){
    ?>
    <script type="text/javascript">
    window.location.href = 'http://localhost/gm-library/views/Book/index.php?errors[]=Make+Sure+to+Check+Books+and+Specify+quentity+to+be+reserved!';
    </script>
  <?php

  }

  if(count($Rbooks) != count($Rbooks2)){
    ?>
    <script type="text/javascript">
    window.location.href = 'http://localhost/gm-library/views/Book/index.php?errors[]=Make+Sure+to+Check+Books+and+Specify+quentity+to+be+reserved!';
    </script>
  <?php
  }
  //var_dump($_POST['Rbooks']);
  $result = array_map(null, $Rbooks, $Rbooks2);



  $UID = auth();
  $time = date('Y-m-d H:i:s');

  $query = "INSERT INTO `bill`(`user_id`, `timestamp`, `status`) VALUES ('{$UID}','{$time}','0')";

  try {
    $conn = Connection::connect();
    $conn->query($query);
  } catch (PDOException $e) {
    echo "error: " . $e->getMessage();
  }

  $query = "SELECT `id` FROM `bill` ORDER BY `id` DESC LIMIT 1";

  try {
    $conn = Connection::connect();
    $id = $conn->query($query);
    $id = $id->fetch();
    $id = $id['id']; 
  } catch (PDOException $e) {
    echo "error: " . $e->getMessage();
  }

  foreach($result as $item) {
    if($item[0] != '' && $item[1] != ''){
    $bid = $item[0];
    $q = $item[1];
    
    $query = "INSERT INTO `bill_contents`(`bill_id`, `book_id`, `quantity`) VALUES ('{$id}','{$bid}','{$q}')";
    try {
      $conn = Connection::connect();
      $conn->query($query);
    } catch (PDOException $e) {
      echo "error: " . $e->getMessage();
    }
    }

  }

  ?>
    <script type="text/javascript">
    window.location.href = 'http://localhost/gm-library/views/Book/index.php?success[]=Reservation+has+been+made+!';
    </script>
  <?php
  
}

if (isset($_GET['search'])) {
  extract($_GET);

  if ($category != "*") {
    $stmt .= " AND `category_id`='{$category}' ";
  }

  if ($author != "*") {
    $stmt .= " AND `author_id`='{$author}' ";
  }
}
$books=[];
try {
  $conn = Connection::connect();
  $books = $conn->query($stmt);
  $books = $books->fetchAll();
} catch (PDOException $e) {
  echo "error: " . $e->getMessage();
}


?>
<section class="container">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Books Index</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">All Books</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
<hr>
  <?php getMessages(); ?>

  <div class="search-box">
    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="GET">
      <div class="row">
        <div class="form-group col col-6">
          <label for="">author</label>
          <select class="form-control" name="author" id="">
            <option>*</option>
            <?php foreach ($authors as $author) : ?>
              <option class="form-control" value="<?php echo $author['id']; ?>"> <?php echo $author['a_name']; ?></option>
            <?php endforeach ?>
          </select>
        </div>
        <div class="form-group col col-6">
          <label for="">category</label>
          <select class="form-control" name="category" id="">
            <option>*</option>
            <?php foreach ($categories as $category) : ?>
              <option class="form-control" value="<?php echo $category['id']; ?>"> <?php echo $category['c_name']; ?></option>
            <?php endforeach ?>
          </select>
        </div>
      </div>
      <input class="btn btn-primary float-right" type="submit" name="search" value="search">
      <div class="clearfix"></div>
    </form>
  </div>
  <hr>
  <!-- Main content -->

  <div class="row">
    <?php foreach ($books as $book) : ?>
      <div class="col-lg-3 col-md-4 col-sm-6 col-12">
        <div class="card ">
          <img class="card-img-top" src="../../images/books/<?= $book['image'] ?>"  />
          <div class="card-body">
            <h5 class="card-title"><?= $book['title']; ?></h5>
            <br><hr>
            <!-- <p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p> -->
            <?php if(auth()): ?>
              <a href="../cart/add.php?book_id=<?= $book['id'] ?>" title="add to cart" class="btn btn-sm btn-success text-white"><i class="fa fa-cart-plus"></i></a>
            <?php endif; ?>
            <span class="card-text ">
              <strong class="text-primary"><?= $book['price'] . "  LYD"; ?></strong>
              <small class="text-muted"><?= $book['quantity'] . " Copy Available"; ?></small>
            </span>
            <!-- <span class="badge badge-success"><i class="fa fa-money"></i> <?= $book['price'] ?></span> -->
            <?php if(auth()): ?>
              <hr>
              <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
                <div class="row">
                  <!-- <input type="hidden" name="id" value="<?= $book['id'] ?>">
                  <div class=" col-lg-10 col-md-12 col-sm-10 col-10">
                    <input class="form-control" type="number" name="quantity" id="" min="1" max="<?= $book['quantity'] ?>" placeholder="Quantity">
                  </div>
                  <input type="checkbox" class="fa-check-double" name="food[]" value="Apple"> -->
                  <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                        <input type="checkbox" name="Rbooks[]" value="<?= $book['id'] ?>">
                        </span>
                      </div>
                      <input class="form-control" type="number" name="Rbooks2[]" id="" min="1" max="<?= $book['quantity'] ?>" placeholder="Quantity">
                  </div> 
                </div>
                <?php endif; ?>
                <?php if(admin()): ?>
                  <hr>
              <div class="btn-group" role="group">
                <a href="edit.php?book_id=<?= $book['id'] ?> " class="btn btn-sm btn-warning" title="Edit" ><i class="fa fa-edit"></i></a>
                <a href="delete.php?book_id=<?= $book['id'] ?> " class="btn btn-sm btn-danger" title="Delete" ><i class="fa fa-trash"></i></a>
              </div>
            <?php endif; ?>
          </div>
          <div class="card-footer">
            <span class="badge badge-info"><i class="fa fa-pen"></i> <?= $book['a_name'] ?></span>
            <span class="badge badge-primary"><i class="fa fa-bookmark"></i> <?= $book['c_name'] ?></span>
          </div>
        </div>
      </div>
    <?php endforeach; ?>
  </div>
  <?php if(auth()): ?>
    <hr>
    <button type="submit" class="btn btn-primary" name="reserve" value="reserve">Reserve Selected Books</button>
    <hr>
  <?php endif; ?>

  </form>

</section>
<!-- /.content -->
<?php require '../layout/footer.php'  ?>
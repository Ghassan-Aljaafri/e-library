<?php
session_start();

require_once("../../models/User.php");
require_once("../../models/Book.php");
require_once("../../models/Cart.php");
require_once("../../settings/Connection.php");
require_once("../../settings/functions.php");

use Models\Book;
use Models\Cart;

if (auth()) {
    if (admin()) {
        try {
            $cart = new Cart();
            $cart->delete($_GET['book_id'] , "book_id");
            $book = new Book();
            $result = $book->delete($_GET['book_id']);
            
            if ($result) {
                header('Location: index.php?success[]=book+deleted+successfully');
            }
        } catch(PDOException $e) {
            echo $stmt . "<br>" . $e->getMessage();
        }
    } else {
        // not admin
        header('Location: index.php?errors[]=you+don\'t+have+a+permission+to+delete');
    }
} else {
    // not auth
    header('Location: ../auth/login.php?errors[]=you+have+to+be+logged+in+first');
}
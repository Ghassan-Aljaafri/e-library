<?php
session_start();

use Models\Author;
use Models\Category;
use Settings\Connection;

require '../layout/header.php';
require '../layout/nav.php';
require '../layout/sidebar.php';

require_once("../../settings/Connection.php");
require_once("../../models/Book.php");
require_once("../../models/Author.php");
require_once("../../models/Category.php");

$authors = new Author();
$authors = $authors->index();
$categories = new Category();
$categories = $categories->index();


if (!admin()) {
    // not admin
    ?>
    <script type="text/javascript">
    window.location.href = 'http://localhost/gm-library/views/Book/index.php?errors[]=you+don\'t+have+a+permission+to+do+that';
    </script>
  <?php

}



if (isset($_POST['add'])) {

    
    // var_dump($_POST);
    extract($_POST);

    if (empty($name) || empty($category) || empty($author) || empty($price) || empty($price)){
      ?>
        <script type="text/javascript">
        window.location.href = 'http://localhost/gm-library/views/Book/create.php?errors[]=Please+fill+required+inputs+!';
        </script>
        <?php
    }

    if(!file_exists($_FILES['photo']['tmp_name']) || !is_uploaded_file($_FILES['photo']['tmp_name'])){
      $imgname = "400px x 600px-r01BookNotPictured.jpg";
    }
    else{
      $imgname = date("hisYdm").$_FILES["photo"]["name"];
      // $imgname = pathinfo($_FILES['photo']['name'], PATHINFO_FILENAME).date("hisYdm");

      $dst="../../images/books/".$imgname;
      move_uploaded_file($_FILES["photo"]["tmp_name"], $dst);
    }

    $stmt = "INSERT INTO `books`(`title`, `category_id`, `author_id`, `price`, `quantity`, `image`) VALUES ('{$name}','{$category}','{$author}','{$price}','{$quantity}','{$imgname}')";

    try {
        $conn = Connection::connect();
        $conn->query($stmt);

        ?>
        <script type="text/javascript">
        window.location.href = 'http://localhost/gm-library/views/Book/index.php?success[]=Book+Has+Been+Added+!';
        </script>
        <?php

      } catch (PDOException $e) {
        echo "error: " . $e->getMessage();
      }
}
?>


<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Add a Book</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Books / Add a Book</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>

  <section class="content">
      <div class="container-fluid">
      <?php getMessages(); ?>
          <!-- left column -->
          <!-- <div class="col-12"> -->
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header bg-primary">
                <h3 class="card-title">Add a Book</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <div class="card-body">
              <form role="form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" enctype="multipart/form-data">
              <div class="row">
                  <div class="form-group col col-12">
                    <label for="name">Book Title</label>
                    <input type="text" name="name" class="form-control" id="name" placeholder="Book's full title" >
                  </div>
                  <div class="form-group col col-6">
                    <label for="category">Category</label>
                    <select name="category" id="category" class="form-control" >
                    <option value selected disabled>-- Select a category --</option>
                    <?php foreach ($categories as $category) : ?>
                      <option class="form-control" value="<?php echo $category['id']; ?>"><?php echo $category['c_name']; ?></option>
                    <?php endforeach ?>
                    </select>
                  </div>
                
                
                
                <div class="col-6">
                  <div class="form-group">
                    <label for="author">Author</label>
                    <select class="form-control" name="author" id="author" >
                        <option value selected disabled>-- Select an author --</option>
                        <?php foreach ($authors as $author) : ?>
                        <option class="form-control" value="<?php echo $author['id']; ?>"><?php echo $author['a_name']; ?></option>
                        <?php endforeach ?>
                    </select> 
                    </div>
                  </div>
                  <div class="col-6">
                  <div class="form-group">
                    <label for="price">Book Price</label>
                    <input class="form-control" type="number"  name="price" id="price" min="1" >
                  </div>
                  </div>
                  <div class="col-6">
                  <div class="form-group">
                    <label for="quantity">َQuantity</label>
                    <input class="form-control" type="number" name="quantity" id="quantity" min="1" >
                  </div>
                  </div>
                  <div class="col-12">
                  <div class="form-group">
                    <label for="photo">Book Cover photo (Optional)</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="photo" name="photo" >
                        <label class="custom-file-label" for="photo">Choose a photo</label>
                      </div>
                    </div>
                  </div>
                  </div>
                <!-- /.card-body -->
                <hr>
                  <button type="submit" class="btn btn-primary" name="add" value="add">Add Book</button>
                  </div>
              </form>
              </div>
              </div>
              </div>
            </div>
          <!-- </div> -->
        </div>
  </section>

  <?php require '../layout/footer.php'  ?>
<?php
session_start();

use Settings\Connection;


require_once("../../settings/Connection.php");
require_once("../../models/Bill.php");
require_once("../../models/User.php");
require_once("../../models/BillContent.php");
require_once("../../settings/functions.php");

if (!auth()) {
    // not auth
    header('Location: ../auth/login.php?errors[]=you+have+to+be+logged+in+first');
} else {
    if (!admin()) {
        // not admin
        header('Location: ../Book/index.php?errors[]=you+don\'t+have+a+permission');
    }
}

$bill = $_GET['bill_id'];

$query = "DELETE FROM `bill_contents` WHERE `bill_id` = {$bill}";
$query2 = "DELETE FROM `bill` WHERE `id` = {$bill}";

if (auth()) {
        try {

            $conn = Connection::connect();
            $conn->query($query);
            $conn->query($query2);

            header('Location: index.php?success[]=Bill+deleted+successfully');

        } catch(PDOException $e) {
            echo $stmt . "<br>" . $e->getMessage();
        }
} else {
    // not auth
    header('Location: ../auth/login.php?errors[]=you+have+to+be+logged+in+first');
}
<?php
session_start();

use Models\Bill;
use Models\BillContent;
use Models\Book;
use Settings\Connection;

require_once("../../settings/Connection.php");
require_once("../../models/Bill.php");
require_once("../../models/Book.php");
require_once("../../models/User.php");
require_once("../../models/BillContent.php");
require_once("../../settings/functions.php");

if (!auth()) {
    // not auth
    header('Location: ../auth/login.php?errors[]=you+have+to+be+logged+in+first');
} else {
    if (!admin()) {
        // not admin
        header('Location: ../Book/index.php?errors[]=you+don\'t+have+a+permission');
    }

}

extract($_GET);

$stmt = "SELECT * FROM `bill` WHERE `id`='{$bill_id}'";

$bill = [];

try {
  $conn = Connection::connect();
  $bill = $conn->query($stmt);
  $bill = $bill->fetch();
} catch (PDOException $e) {
  echo "error: " . $e->getMessage();
}


$billContents = new BillContent();
$billContents = $billContents->showAll($bill['id'], "bill_id");


?>

<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <style>
            @media print {
            #printPageButton {
                    display: none;
                }
            /* @page {
                size: auto; // Hide page links from the print page
                margin: 0;
            } */
            }
        </style>
    </head>
    <body class="col-md-6 col-sm-12 mx-auto my-5">
        <div class="row mb-3">
            <div class="col-12 mb-3"><strong>GM-Library</strong></div>
            <div class="col-md-6 col-sm-12"><strong> Bill Number: </strong><?= $_GET['bill_id'] ?></div>
            <div class="col-md-6 col-sm-12"><strong>UID: </strong><?= auth() ?></div>
        </div>
        <table class="table table-bordered table-striped table-hover table-sm" border="1">
            <thead>
                <tr>
                    <th scope="col">Book ID</th>
                    <th scope="col">Quantity</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($billContents as $billContent) : ?>
                <tr>      
                    <td><?= $billContent['book_id'] ?></td>
                    <td><?= $billContent['quantity'] ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <button id="printPageButton" onclick="window.print()" class="btn btn-sm btn-primary">Print</button>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </body>
</html>
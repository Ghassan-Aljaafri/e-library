<?php 
session_start();

require '../../views/layout/header.php';
require '../../views/layout/nav.php';
require '../../views/layout/sidebar.php';

use Models\Author;
use Models\Bill;
use Models\Book;
use Models\Category;
use Models\User;

require_once("../../settings/Connection.php");
require_once("../../models/Book.php");
require_once("../../models/Author.php");
require_once("../../models/Category.php");
require_once("../../models/Bill.php");

if (!admin()) {
  // not admin
  ?>
  <script type="text/javascript">
  window.location.href = 'http://localhost/gm-library/views/Book/index.php?errors[]=you+don\'t+have+a+permission+to+do+that';
  </script>
<?php

}

$authors = new Author();
$authors = count($authors->index());

$categories = new Category();
$categories = count($categories->index());

$books = new Book();
$books = count($books->index());

$users = new User();
$users = count($users->index());

$bills = new Bill();
$bills = count($bills->index());


 ?>

    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3><?php echo $books ?></h3>

                <p>Books</p>
              </div>
              <div class="icon">
              <i class="ion-ios-book" style="font-size: 55px;"></i>
              </div>
              <!-- <a href="../Book/index.php" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> -->
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3><?php echo $bills ?></h3>

                <p>Reservations</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars" style="font-size: 55px;"></i>
              </div>
              <!-- <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> -->
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3><?php echo $users ?></h3>

                <p>User Registrations</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add" style="font-size: 55px;"></i>
              </div>
              <!-- <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> -->
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">
                <h3><?php echo $authors ?></h3>

                <p>Authors</p>
              </div>
              <div class="icon">
                <i class="ion-android-bookmark" style="font-size: 55px;"></i>
              </div>
              <!-- <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> -->
            </div>
          </div>
          <!-- ./col -->
        </div>
        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    <?php require '../../views/layout/footer.php'  ?>
<?php

use Models\Bill;
use Models\BillContent;
use Models\Book;
use Settings\Connection;

session_start();

require_once("../../settings/Connection.php");
require_once("../../models/User.php");
require_once("../../models/Bill.php");
require_once("../../models/Book.php");
require_once("../../models/BillContent.php");
require_once("../../settings/functions.php");

if (!auth()) {
    // not auth
    header('Location: ../auth/login.php?errors[]=you+have+to+be+logged+in+first');
} else {
    if (!admin()) {
        // not admin
        header('Location: ../Book/index.php?errors[]=you+don\'t+have+a+permission');
    }
}

$bill = new Bill();

$billContentsObject = new BillContent();
$billContents = $billContentsObject->showAll($_GET['bill_id'], 'bill_id');

$book = new Book();
$errorMsgs = "";

foreach ($billContents as $billContent) {
    $quantity = $billContent['quantity'];
    $bookQuantity = $book->showOne($billContent['book_id'])['quantity'];
    if ($quantity > $bookQuantity) {
        $errorMsgs .= "&errors[]=the+book+". $book->showOne($billContent['book_id'])['title'] . "+is+out+of+Stock+"; 

        try {
            $stmt = "DELETE FROM  `bill_contents` WHERE `bill_id` = '{$billContent['bill_id']}' and `book_id` = '{$billContent['book_id']}'";
            $connection = Connection::connect();
            $results = $connection->exec($stmt);
        } catch(PDOException $e) {
            echo $stmt . "<br>" . $e->getMessage();
        }
    
    } else {
        $newQuantity =  $bookQuantity - $quantity;
        $book->update("quantity", $newQuantity, $billContent['book_id']);
    }
}

// if ($errorMsgs == "") {
$result = $bill->update("status", "1", $_GET['bill_id'], "id");

header("Location: reservations.php?success[]=reservation+confirmed{$errorMsgs}");
// } else {
//     header("Location: reservations.php?{$errorMsgs}");
// }
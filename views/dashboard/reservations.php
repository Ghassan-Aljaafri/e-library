<?php
session_start();

use Models\Bill;
use Models\BillContent;
use Models\Book;
use Settings\Connection;

require_once("../../settings/Connection.php");
require_once("../../models/Bill.php");
require_once("../../models/Book.php");
require_once("../../models/User.php");
require_once("../../models/BillContent.php");
require_once("../../settings/functions.php");

if (!auth()) {
    // not auth
    header('Location: ../auth/login.php?errors[]=you+have+to+be+logged+in+first');
} else {
    if (!admin()) {
        // not admin
        header('Location: ../Book/index.php?errors[]=you+don\'t+have+a+permission');
    }
}


$stmt = "SELECT * FROM `bill` WHERE 1=1";

if (isset($_GET['search'])) {
    extract($_GET);
    
    if ($bill_id != "") {
        $stmt .= " AND `id`='{$bill_id}' ";
    }
    
    if ($timestamp != "") {
        $stmt .= " AND date(`timestamp`)='{$timestamp}' ";
    }
    
    if ($status != "") {
        $stmt .= " AND `status`='{$status}' ";
    }
}

$stmt .= " ORDER BY `id` DESC";

$bills = [];

try {
  $conn = Connection::connect();
  $bills = $conn->query($stmt);
  $bills = $bills->fetchAll();
} catch (PDOException $e) {
  echo "error: " . $e->getMessage();
}


// var_dump($bills);
?>

<?php require '../../views/layout/header.php'  ?>
<?php require '../../views/layout/nav.php'  ?>
<?php require '../../views/layout/sidebar.php'  ?>

<div class="container">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            <h1 class="m-0 text-dark">All Reservations</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">All Reservations</li>
            </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <hr>

    <?php getMessages(); ?>
    <div class="search-box">
        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="GET">
        <div class="row">
            <div class="form-group col col-">
                <label for="bill_id">Bill ID</label>
                <input placeholder="Bill ID" class="form-control" name="bill_id" id="bill_id" type="text">
            </div>
            <div class="form-group col col-">
                <label for="timestamp">Date</label>
                <input class="form-control" type="date" name="timestamp" id="timestamp">
            </div>
            <div class="form-group col col-">
                <label for="status">Status</label>
                <select class="form-control" name="status" id="status" >
                    <option value="">Both</option>
                    <option value="1">Aproved</option>
                    <option value="0">Not aproved</option>
                </select>
            </div>
        </div>
        <input class="btn btn-primary float-right" type="submit" name="search" value="search">
        <div class="clearfix"></div>
        </form>
    </div>
    <hr>

    <div class="accordion" id="accordionExample">
        <?php if(count($bills)): foreach ($bills as $bill) : ?>
            <div class="card">
                <div class="card-header bg-light" id="heading<?= $bill['id'] ?>">
                    <h2 class="mb-0">
                        <button class="pl-0 btn btn-link text-bold <?= ($bill['status'])? 'text-success':'text-danger' ?>" type="button" data-toggle="collapse" data-target="#collapse<?= $bill['id'] ?>" aria-expanded="true" aria-controls="collapse<?= $bill['id'] ?>">
                            <i class="fa fa-caret-down"></i> Bill Number #<?= $bill['id'] ?> <i class="fa <?= ($bill['status'])? 'fa-check-square':'fa-times-square' ?>"></i> 
                        </button>
                        <br>
                        <span class="text-muted text-sm"><i class="fa fa-calendar-alt"> <?= $bill['timestamp'] ?></i></span>
                        <div class="float-right">
                            <div class="float-right btn-group">
                                <?php if (!$bill['status']): ?>
                                    <a href="confirmReservation.php?bill_id=<?= $bill['id'] ?>" class="btn btn-sm btn-success"><i class="fa fa-check-double"></i></a>
                                    <a href="canselReservation.php?bill_id=<?= $bill['id'] ?>" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
                                <?php endif; ?>
                                <a href="print.php?bill_id=<?= $bill['id'] ?>" class="btn btn-sm btn-primary"><i class="fa fa-print"></i></a>
                            </div>
                        </div>
                    </h2>
                </div>
                
                <?php
                    $billContents = new BillContent();
                    $billContents = $billContents->showAll($bill['id'], "bill_id");
                ?>
                <div id="collapse<?= $bill['id'] ?>" class="collapse show" aria-labelledby="heading<?= $bill['id'] ?>" data-parent="#accordionExample">
                    <div class="card-body">
                        <ul class="list-group list-group-item-action">
                            <?php foreach ($billContents as $billContent) : ?>
                                <li class="list-group-item list-group-item-action">
                                    <!-- get book title -->
                                    <?php
                                        $book = new Book();
                                        $book = $book->showOne($billContent['book_id']);
                                    ?>
                                    <span class="text-bold"><?= $book['title'] ?></span>
                                    <!-- <?php if (!$bill['status']): ?>
                                        <div class="float-right">
                                            <a href="?book_id=<?= $billContent['book_id'] ?>" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
                                        </div>
                                    <?php endif; ?> -->
                                    <br>
                                    <span class="text-muted text-sm">Quantity: <?= $billContent['quantity'] ?></i></span>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>

        <?php endforeach; else: ?>
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <strong>Sorry!</strong> No Bills found
            </div>
        <?php endif; ?>
    </div>
</div>

<?php require '../../views/layout/footer.php'  ?>
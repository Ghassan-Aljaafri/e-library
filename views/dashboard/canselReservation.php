<?php

use Models\Bill;
use Models\BillContent;

session_start();

require_once("../../settings/Connection.php");
require_once("../../models/User.php");
require_once("../../models/Bill.php");
require_once("../../models/BillContent.php");
require_once("../../settings/functions.php");

if (!auth()) {
    // not auth
    header('Location: ../auth/login.php?errors[]=you+have+to+be+logged+in+first');
} else {
    if (!admin()) {
        // not admin
        header('Location: ../Book/index.php?errors[]=you+don\'t+have+a+permission');
    }
}

$bill = new Bill();
$bill->delete($_GET['bill_id'], "id");

$billContent = new BillContent();
$billContent->delete($_GET['bill_id'], "bill_id");

header("Location: reservations.php?success[]=reservation+canseled");

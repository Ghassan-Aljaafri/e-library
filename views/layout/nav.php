<?php
require_once("../../settings/Connection.php");
require_once("../../models/User.php");
require_once("../../settings/functions.php");
?>

<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-black navbar-dark">
    <!-- Left navbar links -->
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="/gm-library/views/Book/index.php" class="nav-link">Home</a>
      </li>
    </ul>

    <!-- SEARCH FORM -->
    <!-- <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
    </form> -->
    <?php if(auth()): ?>
    <a title="Your Reservations" href="/gm-library/views/reservations/index.php" class="btn btn-lg bg-transparent"><i class="text-warning fa fa-file-invoice"></i></a>
    <a title="Your Cart" href="/gm-library/views/cart/index.php" class="btn btn-lg bg-transparent"><i class="text-warning fa fa-shopping-cart"></i></a>
    <?php endif; ?>
  </nav>
  <!-- /.navbar -->
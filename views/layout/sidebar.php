<?php
require_once("../../settings/Connection.php");
require_once("../../models/User.php");
require_once("../../settings/functions.php");
?>
<!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/gm-library/views/Book/index.php" class="brand-link">
      <img src="/gm-library/images/E-library.png" alt="System Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">E-Library</span>
    </a>
    
    <!-- Sidebar -->
    <div class="sidebar">

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
        <?php if(admin()): ?>
          <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/gm-library/views/dashboard/dashboard.php" class="nav-link">
                  <i class="far fa-eye nav-icon"></i>
                  <p>Main Dashboard</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/gm-library/views/dashboard/reservations.php" class="nav-link">
                  <i class="fa fa-file-invoice-dollar nav-icon"></i>
                  <p>All Reservations</p>
                </a>
              </li>
            </ul>
          </li>
        <?php endif; ?>
        </ul>
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Books
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/gm-library/views/Book/index.php" class="nav-link">
                  <i class="fas fa-align-left nav-icon"></i>
                  <p>All Books</p>
                </a>
              </li>
            <?php if(admin()): ?>
              <li class="nav-item">
                <a href="/gm-library/views/Book/create.php" class="nav-link">
                  <i class="fas fa-plus nav-icon"></i>
                  <p>Add a Book</p>
                </a>
              </li>
            <?php endif; ?>
            </ul>
          </li>
        </ul>
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-user-cog"></i>
              <p>
                Account
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
            <?php if(!auth()): ?>
              <li class="nav-item">
                <a href="/gm-library/views/auth/login.php" class="nav-link">
                  <i class="fa fa-sign-in-alt nav-icon"></i>
                  <p>Login</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/gm-library/views/auth/register.php" class="nav-link">
                  <i class="fa fa-plus nav-icon"></i>
                  <p>Register</p>
                </a>
              </li>
            <?php else: ?>
              <li class="nav-item">
                <a href="/gm-library/views/auth/logout.php" class="nav-link">
                <i class="fa fa-sign-out-alt nav-icon"></i>
                <p>Logout</p>
              </a>
            </li>
            <?php endif; ?>
            </ul>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
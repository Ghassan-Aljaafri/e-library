Creating & Running 'check_Bills' script to check for bills(users's reservations)
that was not confirmed(status = 0) and existed in the system for more than 2 days
from reservations date .

1- first of all , we made the script it self (check_Bills.php) which can be
found in the root of the project .

2- created script.bat file that will tell the windows task scheduler which file to execute as a cronjob.
In this file, i have wrote the path to the php.exe(php interpreter) which is located in my WAMP folder
and the path to the file to be execute as a cronjob (the php script).

inside script.bat => "path_to_php.exe" -f "path_to_check_Bills.php"

3- created a VBS file -> which will tell the path of script.bat ,, created file named shell.vbs .

inside shell.vbs =>

// Starts a new windows shell process (command line).
Set WinScriptHost = CreateObject("WScript.Shell")

//Runs a command and sets the character set for the text string, "script.bat"
WinScriptHost.Run Chr(34) & "script.bat" & Chr(34), 0

//Tells the shell not to display the window.
Set WinScriptHost = Nothing

4- we configured windows task scheduler to run the script :

- open windows task scheduler , from "Actions" menu choose "create task" .
- input a name & decription for the task .
- from the "Triggers" tab we click "New..." to add trigger for the task .
- from "new trigger" window we configure our cronjob to when it will be executed .
- then from "Actions" tab we click "New...".
- in the Program/Script field , we browse for "C:/windows/System32/wscript.exe".
- then in "Add arguments" field , we browse for our shell.vbs .
- finally we click OK, and OK again to close everything .
- Done :) .



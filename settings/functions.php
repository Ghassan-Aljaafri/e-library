<?php

use Models\User;

/**
 * @return user_id if the User logged in.
 * @return false if the User not logged in.
 */ 
function auth() { 
    /**
     * Check if the user is logged in.
     */
    if(!isset($_SESSION['user_id']) || !isset($_SESSION['logged_in'])){
        //User not logged in. Redirect them back to the login.php page.
        return false;
    }
    //User logged in.
    return $_SESSION['user_id'];
}

/**
 * @return True for admin
 * @return False for normal User or Guest
 */
function admin()
{
    if (auth()) {
        $user = new User();
        $user = $user->showOne(auth());
        return $user['role'];
    } else {
        return false;
    }
}

function showErrors($errors)
{
    if (count($errors)) {
        echo "<div class='alert alert-danger' role='alert'>";
        foreach ($errors as $error) {
            echo "<li>". $error . "</li>";
        }
        echo "</div>";
    }
}

function getMessages()
{
    if (isset($_GET['errors'])) {
        echo "<div class='alert alert-danger' role='alert'>";
        foreach ($_GET['errors'] as $error) {
            echo "<li>". $error . "</li>";
        }
        echo "</div>";
    }

    if (isset($_GET['success'])) {
        echo "<div class='alert alert-success' role='alert'>";
        foreach ($_GET['success'] as $success) {
            echo "<li>". $success . "</li>";
        }
        echo "</div>";
    }
}
<?php 
namespace Settings;

use PDO;

require_once("Config.php");

class Connection extends Config {

    // conntection Object
    public static $connection = null;

    private function __construct()
    {
    }

    public static function connect()
    {
        if (self::$connection == null) {
            $address = self::$db['address'];
            $dbname = self::$db['dbname'];
            $dns = "mysql:host={$address};dbname={$dbname}";

            try {
                self::$connection = new PDO($dns, self::$db['username'], self::$db['password']);
                self::$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                // echo 'connect successfully';
                return self::$connection;
            } catch (\PDOException $e) {
                echo "connection faild" . $e->getMessage();
            }
        } else {
            return self::$connection;
        }
    }

    public static function close()
    {
        if (self::$connection != null) {
            self::$connection = null;
            return true;
        } else {
            return false;
        }
    }
}
<?php
namespace Models;

use PDOException;
use Settings\Connection;

// require_once("../settings/Connection.php");

class Model {

    public $table;

    public function index()
    {
        try {
            $stmt = "SELECT * FROM `{$this->table}`";
            $connection = Connection::connect();
            $results = $connection->query($stmt);
            return ($results->fetchAll());
        } catch(PDOException $e) {
            echo $stmt . "<br>" . $e->getMessage();
        }        // Connection::close();
    }
    
    public function showOne($value, $key="id")
    {
        try {
            $stmt = "SELECT * FROM `{$this->table}` WHERE `{$key}` = '{$value}' LIMIT 1";
            $connection = Connection::connect();
            $results = $connection->query($stmt);
            return ($results->fetch());
        } catch(PDOException $e) {
            echo $stmt . "<br>" . $e->getMessage();
        }
        // Connection::close();
    }
    
    public function showAll($value, $key="id")
    {
        try {
            $stmt = "SELECT * FROM `{$this->table}` WHERE `{$key}` = '{$value}'";
            $connection = Connection::connect();
            $results = $connection->query($stmt);
            return ($results->fetchAll());
        } catch(PDOException $e) {
            echo $stmt . "<br>" . $e->getMessage();
        }
        // Connection::close();
    }

    // returns Bool
    public function update($attribute, $newValue, $value, $key="id")
    {
        try {
            $stmt = "UPDATE `{$this->table}` SET `{$attribute}`='{$newValue}' WHERE `{$key}` = '{$value}'";
            $connection = Connection::connect();
            $results = $connection->exec($stmt);
            return $results;
        } catch(PDOException $e) {
            echo $stmt . "<br>" . $e->getMessage();
        }
        // Connection::close();
    }

    // returns Bool
    public function delete($value, $key="id")
    {
        try {
            $stmt = "DELETE FROM  `{$this->table}` WHERE `{$key}` = '{$value}'";
            $connection = Connection::connect();
            $results = $connection->exec($stmt);
            return $results;
        } catch(PDOException $e) {
            echo $stmt . "<br>" . $e->getMessage();
        }
        // Connection::close();
    }
}
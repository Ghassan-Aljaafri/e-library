<?php
namespace Models;

require_once("Model.php");

class Cart extends Model {

    public function __construct()
    {
        $this->table = "cart";
    }
}
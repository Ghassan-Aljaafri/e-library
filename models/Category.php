<?php
namespace Models;

require_once("Model.php");

class Category extends Model {

    public function __construct()
    {
        $this->table = "categories";
    }
}
<?php
namespace Models;

require_once("Model.php");

class Author extends Model {

    public function __construct()
    {
        $this->table = "authors";
    }
}
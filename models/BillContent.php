<?php
namespace Models;

require_once("Model.php");

class BillContent extends Model {

    public function __construct()
    {
        $this->table = "bill_contents";
    }
}
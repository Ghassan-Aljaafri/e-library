<?php
namespace Models;

require_once("Model.php");

class Book extends Model {

    public function __construct()
    {
        $this->table = "books";
    }
}
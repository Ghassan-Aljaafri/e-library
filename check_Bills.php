<?php
session_start();

use Settings\Connection;

require_once("settings/Connection.php");
require_once("models/Bill.php");
require_once("models/User.php");
require_once("models/BillContent.php");
require_once("settings/functions.php");


$time = strtotime(date('Y-m-d H:i:s'));

$query = "SELECT `id`,`timestamp` FROM `bill` WHERE `status`='0'";

try {
    $conn = Connection::connect();
    $bills = $conn->query($query);
    $bills = $bills->fetchAll();
  } catch (PDOException $e) {
    echo "error: " . $e->getMessage();
}

foreach($bills as $bill) {
    $billDate = strtotime($bill['timestamp']);
    $id = $bill['id'];

    $secs = $time - $billDate;// seconds between the two dates
    $days = $secs / 86400;// convert seconds to days

    if($days >= 2){ // if the bill have been in the system for/for more than 2 days , delete it .
        
        $query = "DELETE FROM `bill` WHERE `id`={$id}";

          try {
              $conn = Connection::connect();
              $conn->query($query);
            } catch (PDOException $e) {
              echo "error: " . $e->getMessage();
            }

    }

}

// header("Location: views/Book/");


